import logging
import os
import random

from pyaxepta import Axepta

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger()

AXEPTA_ID = os.getenv('AXEPTA_ID', '08000001')
AXEPTA_KEY = os.getenv('AXEPTA_KEY', 'xHosiSb08fs8BQmt9Yhq3Ub99E8=')

RANDOM_SHOP_ID = ''.join(random.choice('0123456789ABCDEF') for i in range(16))
RANDOM_TOKEN = ''.join(random.choice('0123456789ABCDEF') for i in range(16))


class TestGestpay:

    def setup_method(self, method):
        self.axepta = Axepta(tid=AXEPTA_ID, ksig=AXEPTA_KEY, test=True)
        self.transaction_id = RANDOM_SHOP_ID
        self.token = RANDOM_TOKEN

    def test_card_transaction_success(self):
        data = {
            'amount': 100,
            'transaction_id': self.transaction_id,
            'card_number': '4111111111111111',
            'exp_month': 10,
            'exp_year': 2023,
            'cvv2': 1234,
            'payInstrToken': self.token,  # register to use later
        }

        res = self.axepta.card_transaction(**data)
        assert res['rc'] == 'IGFS_000'

    def test_card_transaction_failure(self):
        data = {
            'amount': 100,
            'transaction_id': 'failingTransactionID',
            'card_number': '5430131234567891',
            'exp_month': 10,
            'exp_year': 2023,
            'cvv2': 1234,
        }

        res = self.axepta.card_transaction(**data)
        assert res['rc'] == 'IGFS_008'  # 'AUTORIZZAZIONE NEGATA'

    def test_token_transaction_success(self):
        data = {
            'amount': 100,
            'transaction_id': self.transaction_id,
            'token': self.token,
        }

        res = self.axepta.token_transaction(**data)
        assert res['rc'] == 'IGFS_000'

    def test_delete_token(self):
        data = {
            'transaction_id': self.transaction_id,
            'token': self.token
        }

        res = self.axepta.delete_token(**data)
        assert res['rc'] == 'IGFS_000'
